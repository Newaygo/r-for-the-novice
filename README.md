# Building Navigation platform prototype utilizing Anonymous Identifiers programming with R, XHTML, Dplyr, CRAN, Dtplyr, tidyverse

Using Anonymous Identifiers to organize, filter, sort, segment and navigate visiting citizen audience traffic using R, XHTML and yet-to-be-determined database

We intend to meet online for a peer-to-peer study group, and application prototype working/building group alongside the study group, meeting online for 2 hrs each week for a minimum of 8 weeks.

It is intended to build a prototype for directing participating audience member traffic to alternative, yet, most appropriate options based on their expressed preferences.

Utilize learnr R package to turn any R Markdown document into an interactive tutorial. Tutorials consist of content along with interactive components for checking and reinforcing understanding.
Tutorials automatically preserve work done within them, so if a user works on a few exercises or questions and returns to the tutorial later they can pick up right where they left off.

Utilize pkgdown to make it quick and easy to build a website for your package.

Utilize dtplyr to provide a data.table backend for dplyr, allowing creator to write dplyr code that is automatically translated to the equivalent data.table code.